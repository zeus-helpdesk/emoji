#!/bin/bash

mkdir ../64
find ./twemoji/2/svg -name "*.svg" -exec sh -c 'inkscape "$1" -e "${1%.svg}.png" -w 64 -h 64' _ {} \;
find ./twemoji/2/svg -name "*.png" -exec mv -t ../64 {} +

mkdir ../72
find ./twemoji/2/svg -name "*.svg" -exec sh -c 'inkscape "$1" -e "${1%.svg}.png" -w 72 -h 72' _ {} \;
find ./twemoji/2/svg -name "*.png" -exec mv -t ../72 {} +
